using UnityEngine;
using System.Collections;

public class CapsuleRotate : MonoBehaviour {
	/**
	 * This is the axis the capsule "points" in
	 * For capsules, it's most intuitive that it's along the length of it
	 * It seems to be in the y-axis, but I can't really tell
	 * Anyways adjust this as needed
	 */
	private Vector3 backbone_axis = new Vector3(0, 1, 0);
	
	/**
	 * This is the axis the capsule rotates around
	 */
	private Vector3 rotation_axis = new Vector3(0, 0, -1);
	
	/**
	 * This is how fast the capsule rotates
	 * Higher is faster
	 */
	private const float ANGLE_SCALE = 35;
	
	/**
	 * This is how fast the capsule moves up and down
	 * Higher is faster
	 */
	private const float VERTICAL_SCALE = 0.5f;
	
	private const string ORBIT_PREFAB = "prefabs/PrefabOrbitSphere";
	private const string ORBIT_PREFAB_COMPONENT = "OrbitCircleSatellite";
	private const float ORBIT_RADIUS = 3;
	private const float ORBIT_SPEED = 12;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire2")) {
			this.CreateOrbitingSphere();
		}
		this.MoveHorizontal(Input.GetAxis("Horizontal"));
		this.MoveVertical(Input.GetAxis("Vertical"));
	}
	
	void CreateOrbitingSphere() {
		/**
		 * Orbit around the center of this object
		 * The axis of the orbit is based on the backbone
		 */
		OrbitCircleSphereFactory.CreateOrbiter(
			CapsuleRotate.ORBIT_PREFAB,
			CapsuleRotate.ORBIT_PREFAB_COMPONENT,
			this.gameObject,
			this.backbone_axis,
			CapsuleRotate.ORBIT_RADIUS,
			CapsuleRotate.ORBIT_SPEED);	
	}
	
	void MoveVertical(float vertical) {
		if (vertical == 0) {
			return;
		}
		
		this.transform.position = new Vector3(
			this.transform.position.x,
			this.transform.position.y + (vertical * CapsuleRotate.VERTICAL_SCALE),
			this.transform.position.z);
	}
	
	void MoveHorizontal(float horizontal) {
		float delta_rotate;
		
		if (horizontal == 0) {
			return;
		}
		
		/* TODO: need to check the handedness of this*/
		delta_rotate = CapsuleRotate.ANGLE_SCALE * Time.deltaTime * horizontal;
		this.transform.RotateAround(
		 		this.transform.position,
		 		this.rotation_axis,
				delta_rotate);
	}
}
