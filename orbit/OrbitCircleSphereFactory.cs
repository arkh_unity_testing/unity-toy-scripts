using UnityEngine;
using System.Collections;

/**
 * This obvs is a factory for creating orbiting objects
 * CreateOrbiter's params should be self explanatory
 */
public class OrbitCircleSphereFactory {
	/**
	 * This is mostly for testing purposes to see that the sphere moves correctly
	 * into the plane of the circle.
	 */
	private static Vector3 POS_JITTER = new Vector3(1.0f, 1.0f, 1.0f);
	
	/**
	 * @param prefab: obvs the prefab to use; do it when the resource is already loaded
	 * @param prefab_orbit_component: Name of the component in the prefab that implements IOrbitCircle
	 * @param center: the object at the center of the orbit
	 * @param orbit_axis: the default axis of the object to rotate around
	 * Note that this is before the center object's rotations is applied
	 * e.g. for a sphere, if its default "up" is the y-axis, maybe do
	 * this.transform.rotation*(new Vector3(0,1,0))
	 * @param linear_speed: This is the linear speed (i.e. not angular speed).
	 */
	public static void CreateOrbiter (
			UnityEngine.Object prefab,
			string prefab_orbit_component,
			GameObject center,
			Vector3 orbit_axis,
			float orbit_radius,
			float linear_speed) {
		GameObject orb;
		OrbitCircle oc;
		IOrbitCircle o;
		
		orb = (GameObject)Object.Instantiate(prefab,
			center.transform.position + OrbitCircleSphereFactory.POS_JITTER, 
			Quaternion.identity);
		if (orb == null) {
			return;
		}
		
		Physics.IgnoreCollision(orb.collider, center.collider);
		
		/* Create a new OrbitCircleWavy by using the following line instead */
		//oc = new OrbitCircleWavy(center, orbit_axis, orbit_radius, linear_speed);
		 
		oc = new OrbitCircle(center, orbit_axis, orbit_radius, linear_speed);
		o = (IOrbitCircle)orb.GetComponent(prefab_orbit_component);
		if (o == null) {
			return;
		}
		o.oc = oc;
	}
	
	public static void CreateOrbiter (
			string prefab_name,
			string prefab_orbit_component,
			GameObject center,
			Vector3 orbit_axis,
			float orbit_radius,
			float linear_speed) {
		OrbitCircleSphereFactory.CreateOrbiter(
			Resources.Load (prefab_name), prefab_orbit_component,
			center, orbit_axis, orbit_radius, linear_speed);
	}
}
