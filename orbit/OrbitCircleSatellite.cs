using UnityEngine;
using System.Collections;

/**
 * Example of an object that would orbit something
 * For any object that only needs to orbit, just add this to the object
 */
public class OrbitCircleSatellite : MonoBehaviour, IOrbitCircle {
	private OrbitCircle oc_ref = null;
	
	public OrbitCircle oc {
		set { this.oc_ref = value; }
		get { return this.oc_ref; }
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this.oc == null) {
			return;
		}
		
		this.transform.position = this.oc.GetNextPos(this.transform.position, Time.deltaTime);
	}
}
